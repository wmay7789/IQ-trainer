# IQ Trainer

This is a brain training app, in which the questions are based off several different standard IQ tests. It contains a matrices generators, randomized questions based off the Standford Binet, Adult Langdan test as well as the Mega test.